#!/bin/bash

rm -rf ./results/*

# Script assumes $1 is the hosts.ini file usually fed to ansible

ansible-playbook m-queens-test.yml -i "$1"

find ./results/ -name "result.txt" -exec ls {} \; | sort

cat "$(find ./results/ -name "result.txt" -exec ls {} \; | sort)"

tar cf ./results.tar.bz2 ./results/ --use-compress-program='lbzip2 -n 4'
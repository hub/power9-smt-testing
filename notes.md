# Notes from SMT testing

Using the command 'ppc64_cpu' to alter the kernel's use of SMT on the fly will
turn off/on SMT for the host, but not any KVM guests.

Upon turning off SMT, and then back on, virtual machines will (unfortunately)
be "tagged to" or stuck on the physical cores' IDs (0,4,8,12,etc.).

At the time of this writing, if one has to disable SMT and re-enable it, a
reboot will be required to get guest VMs the proper resources they need on the CPUs.
